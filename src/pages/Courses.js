//import useState,useEffect,useContext
import {useState,useEffect,useContext} from 'react'
 
//import the Banner Component
import Banner from '../components/Banner';
//import the CourseCard component
import CourseCard from '../components/CourseCard';
import AdminDashboard from '../components/AdminDashboard';
 
//import our user context
import UserContext from '../userContext';
 
const Courses = () => {
 
   const {user} = useContext(UserContext);
   //console.log(user);
 
   //console.log(coursesData);
 
   let coursesBanner = {
 
       title: "Welcome to the Courses Page.",
       description: "View one of our courses below.",
       buttonText: "Register/Login to Enroll",
       destination: "/register"
 
   }
 
   //create a state to save our course components for all active course.
   const [coursesArray,setCoursesArray] = useState([])
 
   //create useEffect which will retrieve our courses on initial render.
   useEffect(()=>{
 
 
       //fetch all ACTIVE courses.
       //For requests that are GET method AND does not need to pass token,
       //we don't need to add {options}.
       fetch("http://localhost:4000/courses/getActiveCourses")
       .then(res => res.json())
       .then(data => {
 
           //console.log(data);
 
           //create a new array of course components out of our array of documents (data):
           setCoursesArray(data.map(course => {
 
               return (
 
                       <CourseCard key={course._id} courseProp={course} />
 
                   )
 
           }))
 
       })
 
 
   },[])
 
   //console.log(coursesArray);
 
 
 
  
 
   /*Create an array of CourseCard components by mapping out details from our coursesData array*/
/*  let coursesComponents = coursesData.map(course => {
 
       //we essentially returned an array of CourseCard components into our new courseComponents.
 
       //We are also able to pass the data of the current item being looped by map(), by passing the course parameter in our anonymous function
 
       //When we create an array of components, we have to add a unique identifier for each component. Pass a key prop with a unique identifier for each item in the array
 
       return <CourseCard courseProp={course} key={course.id}/>
 
   })
*/
   //console.log(coursesComponents)
 
 
   return (
 
       user.isAdmin
       ?
       <AdminDashboard />
       :
       <>
           <Banner bannerProp={coursesBanner}/>
           {coursesArray}
       </>
 
 
   )
}
export default Courses;