//Our home page component will be the parent component of our banner and highlights components.
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
 
export default function Home(){
 
   /*
 
       ReactJS adheres to the concept of D.R.Y. - Don't Repeat Yourself.
 
       Components in ReactJS are independent and reusable.
 
       What makes a ReactJS component reusable is with the use of props.
 
       Props are data we can pass from a parent component to a child component.
 
       Parent components are components which return other components.
 
       Child components are components returned by a parent component.
 
   */
 
   //let sampleProp = "I am sample data passed from Home component to Banner Component."
 
   /*
       To pass prop from a parent component to a child component, we add HTML-like attribute to the child component which we can name ourselves. Props are html-like attributes we can name ourselves.
 
       The name of the attribute will become the name of a property of the object that the child component receives. That is why this is called Props. Props stand for properties.
 
   */
 
 
 
   let sampleProp2 = "This sample data is passed from Home to Highlights component."
 
   //object to be passed as props
 
   let bannerData = {
 
       title: "Zuitt Booking System B146",
       description: "View and book a course from our catalog!",
       buttonText: "View Our Courses",
       destination: "/courses"
 
   }
 
 
   return (
 
       <>
           <Banner bannerProp={bannerData}/>
           <Highlights highlightsProp={sampleProp2}/>
       </>
 
   )
 
}