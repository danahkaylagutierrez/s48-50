//import useContext() hook
import {useContext} from 'react';
 
//import react-bootstrap components:
import {Nav,Navbar,Container} from 'react-bootstrap'
 
//import user context:
import UserContext from '../userContext';
 
//import Link from react-router-dom to be able to use links that will not refresh our pages when switching pages.
import {Link} from 'react-router-dom'
 
const AppNavBar = () => {
 
   /*
       useContext() hook will allow us to unwrap the values provided by our UserProvider from our UserContext. useContext() will return an object after unwrapping our context.
  
   */
 
   //console.log(useContext(UserContext));
  
   //destructure our UserContext to get the global user state.
   const {user} = useContext(UserContext);
   console.log(user);
 
   return (
 
           <Navbar bg="dark" variant="dark" expand="lg">
               <Container fluid>
                   <Navbar.Brand href="/">B146 Booking</Navbar.Brand>
                   <Navbar.Toggle aria-controls="basic-navbar-nav" />
                   <Navbar.Collapse id="basic-navbar-nav">
                       <Nav className="ml-auto">
                       <Link to="/" className="nav-link">Home</Link>
                           <Link to="/courses" className="nav-link">Courses</Link>
                           {
                               user.id
                               ?
                                   user.isAdmin
                                   ?
                                   <>
                                       <Link to="/addCourse" className="nav-link">Add Course</Link>
                                       <Link to="/logout" className="nav-link">Logout</Link>
                                   </>
                                   :
                                   <Link to="/logout" className="nav-link">Logout</Link>
                               :
                               <>
                                   <Link to="/register" className="nav-link">Register</Link>
                                   <Link to="/login" className="nav-link">Login</Link>
                               </>
                           }
                       </Nav>
                   </Navbar.Collapse>
               </Container>
           </Navbar>
 
 
       )
 
}
export default AppNavBar;
