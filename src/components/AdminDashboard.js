import {useState,useEffect,useContext} from 'react';
 
import UserContext from '../userContext';
 
import {Table,Button} from 'react-bootstrap';
 
import {Navigate} from 'react-router-dom';
 
export default function AdminDashboard(){
 
   //unwrap our global user state
   const {user} = useContext(UserContext);
   console.log(user)
 
   //state for allCourses
   const [allCourses,setAllCourses] = useState([]);
 
	//activate function
   function activate(courseId){
       fetch(`http://localhost:4000/courses/activate/${courseId}`,{
 
       method: 'PUT',
       headers: {
           'Authorization': `Bearer ${localStorage.getItem('token')}`
       }
 
   })
   .then(res => res.json())
   .then(data => {
 
       console.log(data);
       //refresh the page after archive
       window.location.href = "/courses";
 
   })
 
   }




   //archive function
   function archive(courseId){
 
       //console.log(courseId);
       //When the archive button per course is clicked,
       //the id of the course is passed here in the archive function.
       //the archive function will then use fetch to create a request to archive our course.
       fetch(`http://localhost:4000/courses/archive/${courseId}`,{
 
           method: 'PUT',
           headers: {
               'Authorization': `Bearer ${localStorage.getItem('token')}`
           }
 
       })
       .then(res => res.json())
       .then(data => {
 
           console.log(data);
           //refresh the page after archive
           window.location.href = "/courses";
 
       })
 
 
   }
 
 
   //useEffect to fetch the details of all courses. Add empty dependency array so the useEffect will only run on initial render.
   useEffect(()=>{
 
       if(user.isAdmin){
 
           //fetch all courses
           fetch('http://localhost:4000/courses/',{
 
               headers: {
                   'Authorization': `Bearer ${localStorage.getItem('token')}`
               }
 
           })
           .then(res => res.json())
           .then(data => {
 
               //console.log(data);
               setAllCourses(data.map(course => {
 
                   return (
 
                           <tr key={course._id}>
                               <td>{course._id}</td>
                               <td>{course.name}</td>
                               <td>{course.price}</td>
                               <td>{course.isActive ? "Active": "Inactive"}</td>
                               <td>
                                   {
                                       course.isActive
                                       ?
                                       <Button variant="danger" className="mx-2" onClick={()=>{archive(course._id)}}>Archive
                                       </Button>
                                       :
                                       <Button variant="success" className="mx-2">
                                           Activate
                                       </Button>
                                   }
                               </td>
                           </tr>
 
 
                       )
 
               }))
           })
 
       }
 
   },[])
 
   //If you update a state with a setter function, it re-renders the component/runs the component again. If you update a state with the setter function outside of a useEffect or a function, it will cause infinite loop.
 
   //setUpdate(update+1);
 
 
   return (
       <>
           <h1 className="my-5 text-center">Admin Dashboard</h1>
           <Table striped bordered hover>
               <thead>
                   <tr>
                       <th>ID</th>
                       <th>Name</th>
                       <th>Price</th>
                       <th>Status</th>
                       <th>Actions</th>
                   </tr>
               </thead>
               <tbody>
                   <tr>
                       <td>idSample</td>
                       <td>nameSample</td>
                       <td>priceSample</td>
                       <td>statusSample</td>
                       <td>actionsSample</td>
                   </tr>
                       {allCourses}
               </tbody>
           </Table>
       </>
       )
 
}
