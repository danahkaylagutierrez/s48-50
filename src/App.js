


//When creating a react component, we first import react as a package.
//However, since fragments no longer need to be coming from React, we won't need to import anymore. This is because in the latest update of React, React is no longer needed to be import to create your components. However, if there are components from React that you need to use, you should still import React.
//import is similar to require() in express js.
import {useState,useEffect} from 'react';
 
//import react-bootstrap components
import {Container} from 'react-bootstrap';
 
//import react-router-dom components to simulate/implement page routing in our app.
import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom'
 
//import components - to use components in another component/page, first import them.
import AppNavBar from './components/AppNavBar';
 
 
//import pages
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/ErrorPage';
import AddCourse from './pages/AddCourse';
import Logout from './pages/Logout';
import ViewCourse from './pages/ViewCourse';
 
//import user provider from our usercontext;
import { UserProvider } from './userContext';
 
//import css
import './App.css';
 
//Components should be able to return something or react to elements.
//If you want to return a blank page, return null.
//Even in your components, add react fragments when returning 2 or more adjacent elements.
//Import and render/return components inside other components.
export default function App(){
 
 /*
   Reactjs was first conceptualized to be used on SPA (single page application). So instead, we will simulate within our Reactjs the switching of pages.
 
   react-router-dom - allows us to simulate page switching for our Reactjs app.
 
   npm install react-router-dom
 
   We have 3 components to simulate/implement routing in our Reactjs app.
 
   Router
     -Is wrapped around all components which will have access to our routing system.
 
   Routes
     -holds all of our Route components.
 
   Route
     -assigns an endpoint and displays the appropriate page component for that endpoint.
       -path attribute assigns the endpoint.
       -element attribute assigns the Page Component to be displayed at that endpoint.
 
 */
 /*
   Wrap all components with our UserProvider component to enable our components to access data from our UserContext. Any component not inside UserProvider will not have access to the values we provide from our UserProvider.
 
   we also add a value attribute to our UserProvider. This value attribute will be the data or functions that are stored within our UserContext.
 
 
 */
 
 const [user,setUser] = useState({
 
     id: null,
     isAdmin: null
 
 })
 
 /*
   useEffect allows us to create effects. These effects are anonymous functions we can run at our control. We can have a useEffect run only once on initial render (the first time the component ran.), or run useEffect when the component re-renders, or run a useEffect when a particular state or particular states are updated.
 
   useEffect(()=>{},[dependency array determines WHEN our useEffect will run.])
 
   1. if the dependency array is empty it will ONLY on initial render.
  
   2. if the dependency array contains variables/states, it will run ONLY when the variable/state is updated AND on initial render.
 
   3. if there is no dependency array, it will run any time a state is updated AND on initial render.
 
   useEffect always runs on initial render.
 
 */
 
 useEffect(()=>{
 
   //fetch() will run and get our userDetails whenever the page refresh and our main component initially renders.
   fetch('http://localhost:4000/users/getUserDetails',{
 
     method: 'GET',
     headers: {
 
       'Authorization': `Bearer ${localStorage.getItem('token')}`
 
     }
 
   })
   .then(res => res.json())
   .then(data => {
 
     //console.log(data);
     setUser({
 
       id: data._id,
       isAdmin: data.isAdmin
 
     })
 
   })
 
 },[])
 
 const unsetUser = () => {
 
   //localStorage.clear() allows us to clear our localStorage.
   localStorage.clear()
 
 }
 
 return (
    <>
     <UserProvider value={{user,setUser,unsetUser}} >
       <Router>
         <AppNavBar />
         <Container>
           <Routes>
             <Route path="/" element={<Home />} />
             <Route path="/courses" element={<Courses />} />
            <Route path="/courses/viewCourse/:courseId" element={<ViewCourse />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/addCourse" element={<AddCourse />} />
           </Routes>
         </Container>
       </Router>
     </UserProvider>
   </>
 
 )
 
}
